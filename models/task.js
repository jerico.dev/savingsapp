const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    description: {
        type: String,
        required: [true, 'Task description is required']
    },
    increment: {
        type: Number,
        required: [true, 'Increment is required']
    },
    total:{
        type: Number,
        required: [true, 'Task total is required']
    },
    startDate: {
        type: Date,
        required: [true, 'Start date is required']
    },
    endDate: {
        type: Date,
        required: [true, 'End date is required']
    },
    duration: {
        type: Number,
        required: [true, 'Task duration is required']
    },
    user: [{
        userId: {
            type: String,
            required: [true, 'User ID is missing for this task']
        },
        userName: {
            type: String,
            required: [true, 'User name is missing for this task']
        }
    }]
});

module.exports = mongoose.model('Task', taskSchema);
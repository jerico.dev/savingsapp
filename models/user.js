const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    userName: {
        type: String,
        required: [true, 'Username is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    tasks: [{
        taskId: {
            type: String,
            required: [true, 'Task ID was not pushed']
        },
        taskDescription: {
            type: String,
            required: [true, 'Task description was not pushed']
        },
        taskStartDate: {
            type: String,
            required: [true, 'Task start date was not pushed']
        },
        taskEndDate: {
            type: String,
            required: [true, 'Task end date was not pushed']
        },
        taskTotal: {
            type: Number,
            required: [true, "Task Total is was not pushed"]
        },
        taskBalance: {
            type: Number,
            required: [true, "Task Balance was not pushed"]
        },
        taskDuration: {
            type: Number,
            required: [true, 'Task duration was not pushed']
        },
        breakdown: [{
            week: {
                type: Number,
                required: [true, "Week is missing for the user's task breakdown"]
            },
            taskExpectedAmount: {
                type: Number,
                required: [true, 'Task expected amount was not pushed']
            },
            taskTotalAmount: {
                type: Number,
                required: [true, 'Task total amount was not pushed']
            },
            isPaid: {
                type: Boolean,
                default: false
            },
            payment: {
                type: Number,
                default: 0
            }
        }],
        progress: {
            type: Number
        }
    }]

});

module.exports = mongoose.model('User', userSchema);

const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/user");

// Registration
module.exports.register = (req, res) => {
    
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        userName: req.body.userName,
        password: bcrypt.hashSync(req.body.password, 10)
    })

    User.findOne({userName: req.body.userName})
        .then(result => {
            if(result){
                res.status(404).json("Username already used")
            }
            else{
                newUser.save()
                .then(user => res.status(200).json(user))
                .catch(error => {
                    console.error('Registration Error: ', error)
                    res.status(500).json(error)
                })
            }
        })

    
}

// Login
module.exports.login = async (req, res) => {

    await User.findOne({userName: req.body.userName})
        .then(result => {
            if(result == null){
                res.status(400).json("User doesn't exist")
            }
            else{
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

                if(isPasswordCorrect){
                    res.status(200).json(auth.createToken(result))
                }
                else{
                    res.status(400).json('Username or Password Incorrect')
                }
            }
        })
        .catch(err => {
            console.error('Login Error: ', err);
            res.status(500).json(err)
        })

}

// Fetching the user details
module.exports.getUserInfo = async (req, res) => {
    const userToken = auth.decode(req.headers.authorization)
    
    await User.findById(userToken.userId)
        .then(result => {
            if(result){
                res.status(200).json(result)
            }
            else{
                res.status(404).json('No user found')
            }
        })
        .catch(err => {
            console.error('Error fetching user profile', err)
            res.status(500).json(err)
        })
        
}

// To empty the task array of the user
module.exports.clearUserTaskArray = async (req, res) => {

    const token = auth.decode(req.headers.authorization);
  
    await User.findByIdAndUpdate(token.userId, {$set: {tasks: []}}, {new: true})
        .then(result => {
            if(result){
                res.json(result);
            }
            else{
                res.status(404).json("Unable to verify the token");
            }
            
        })
        .catch(err => {
            console.error('Error Clearing the Task Array: ', err);
            res.status(500).json(err)
        })
    
}
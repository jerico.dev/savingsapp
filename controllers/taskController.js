const userController = require('./userController');
const Task = require('../models/task');
const User = require('../models/user');
const auth = require('../auth');


module.exports.createTask = async (req, res) => {

    const loggedUser = auth.decode(req.headers.authorization);
  
    let start = new Date(req.body.startDate);
    let end = new Date(req.body.endDate);
    let days = Math.floor((end-start)/(1000*3600*24))
    let weeks = Math.floor(days/7);
    let inputAmount = req.body.increment;
    let currentValue = 0;
    let previousValue = 0; 
    let userTaskBreakdown = [];

    for(week=0; week<weeks; week++){
        currentValue += inputAmount                     // To get the current amount for a specific week
        previousValue = currentValue+previousValue;     // To store the total amount of the increments 
        
        // This will update the user task array to see the specific details of task per week
        userTaskBreakdown.push({                        
            week: week+1,
            taskExpectedAmount: currentValue,
            taskTotalAmount: previousValue

        })
    }
    

    let newTask = new Task({
        description: req.body.description,
        increment: inputAmount,
        total: previousValue,
        startDate: start,
        endDate: end,
        duration: weeks,
        user: [{
            userId: loggedUser.userId,
            userName: loggedUser.userName
        }]
    })
    
    await User.findById(loggedUser.userId) // to validate if the token is correct
        .then(result => {

            if(result){
                if(days % 7 === 0){     // Added this validation to get full week and calculate the total value based on weeks
                    newTask.save()
                    .then(result => {
            
                        User.findById(loggedUser.userId).then(foundUser => {
                            
                            foundUser.tasks.push({
                                taskId: result._id,
                                taskDescription: result.description,
                                taskStartDate: result.startDate,
                                taskEndDate: result.endDate,
                                taskTotal: newTask.total,
                                taskBalance: newTask.total,
                                taskDuration: result.duration,
                                breakdown: userTaskBreakdown,
                                progress: 0
                            })
            
                            foundUser.save()
                            .then(updatedUser => {})
                            .catch(err => {
                                console.error("User's Tasks Array is not Updated", err)
                            })
                        })
                        res.status(200).json(result)
                    })
                    .catch(err => {
                        console.error('Error Creating task', err)
                        res.status(500).json(err)
                    })
                }
                else{
                    res.status(400).json('Please select a date range that has complete 7 days in a week.')
                }
            }
            else{
                res.status(500).json("Unable to authenticate")
            }
        })
        .catch(error => {
            console.error('Error: ', error)
            res.status(500).json(error)
        })
   
    
}

module.exports.displayTaskBreakdown = (req, res) => {
    const userToken = auth.decode(req.headers.authorization);
    const taskId = req.params.taskId;

    User.findById(userToken.userId)
        .then(result => {
            
            if(result){
                let matchTask = result.tasks.filter(filteredTask => filteredTask.taskId === taskId);
         
                return res.json(matchTask)
            }
            else{
                return res.json("Invalid Token")
            }

        })
        .catch(err => {
            console.error('Could not display task/s: ', err)
        })
}

module.exports.updateTask = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    const mainTaskId = req.params.mainTaskId;
    const weeklyTaskId = req.params.weeklyTaskId;
    
    
    await User.findById(userData.userId)
        .then(userFound => {

            if(userFound){
                let taskFound = userFound.tasks.find(task => task.taskId === mainTaskId);
                let filteredTask = taskFound.breakdown.filter(sorted => sorted._id == weeklyTaskId);
                let breakdownArray = [];
                
                
                filteredTask.map(status => status.isPaid = req.body.isPaid)
                
                // let paidCount = taskFound.breakdown.filter(paidCount => (paidCount.isPaid == true)).length;
                // let taskArrayLength = taskFound.breakdown.length;
                let progressRatio;

                if(req.body.isPaid == true){
                    
                    taskFound.taskBalance = taskFound.taskBalance - filteredTask[0].taskExpectedAmount
                    progressRatio = ((taskFound.taskTotal - taskFound.taskBalance)/taskFound.taskTotal)*100
                   
                }
                else if(req.body.isPaid == false){
                    
                    taskFound.taskBalance = taskFound.taskBalance + filteredTask[0].taskExpectedAmount
                    progressRatio = ((taskFound.taskTotal - taskFound.taskBalance)/taskFound.taskTotal)*100
                    
                }
                
                
                taskFound.progress = Math.ceil(progressRatio);

                userFound.save()

                .catch(err => console.log("Error Updating Payment Status: ", err))
                
                return res.json(taskFound)
            }
            else{
                return res.json("Authentication failed")
            }

        })
        .catch(err => {
            console.error("Error fetching user data: ", err)
            return res.json(err)
        })
}

module.exports.deleteAllTasks = (req, res) => {
    // const token = auth.decode(req.headers.authorization); ------> will add this auth when frontend is available
    return Task.deleteMany()
        .then(result => {
            
            res.status(200).json(result)
        })
        .catch(err => {
            console.error('Deletion Error: ', err)
            res.status(500).json(err)
        })
}
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoute = require('./routers/userRoute');
const taskRoute = require('./routers/taskRoute');

// creates the express application
const app = express();

// Middlewares - allows to bridge our backend application (server) to our front end
// to allow cross origin resource sharing
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended: true}));

app.use('/users', userRoute);
app.use('/tasks', taskRoute);

mongoose.connect("mongodb+srv://eco62:eco62@portfolio.ux0xyzs.mongodb.net/SavingsApp?retryWrites=true&w=majority")
.catch(err => console.log('Connection to DB Error: ', err))

mongoose.connection.once("open", () => console.log("Connected to Magcale's atlas"))

app.listen(process.env.PORT || 3003, () => console.log(`Now connected to ${process.env.PORT || 3003}`));
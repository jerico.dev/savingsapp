const jwt = require('jsonwebtoken');
const secretKey = 'savingsChallenge';

// Token Creator
module.exports.createToken = user => {

    // Payload
    const data = {
        userId: user._id,
        userName: user.userName
    }

    return jwt.sign(data, secretKey)
};


// Token verifier - verify token from Postman
module.exports.verifyToken = (request, response, next) => {
    
    // Get jwt from Postman 
    let token = request.headers.authorization;

    if(token !== undefined){

        // removes the first 7 character ("Bearer ") from the token
        token = token.slice(7, token.length)

        return jwt.verify(token, secretKey, error => {
            if(error){
                return response.send({
                    auth: 'Authentication failed'
                });
            }
            else{
                next();
            }
        });
    }
    else{
        response.status(500).send("Couldn't verify token")
    }
}


// Token decoder - to decode the data from the token
module.exports.decode = token => {

    if(token){
        token = token.slice(7, token.length);

        return jwt.verify(token, secretKey, error => {
    
            if(error){
                return ({
                    Auth: 'Decode failed'
                });
            }
            else{
                return jwt.decode(token, {complete:true}).payload
            }
        })
    }
    else{
        return ('Authentication failed')
    }
    
}
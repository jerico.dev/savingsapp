const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');

router.post('/createTask', taskController.createTask);
router.get('/:taskId', taskController.displayTaskBreakdown)
router.patch('/:mainTaskId/:weeklyTaskId', taskController.updateTask)

router.delete('/deleteAllTasks', taskController.deleteAllTasks)

module.exports = router;
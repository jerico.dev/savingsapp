const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.post('/registration', userController.register);
router.post('/login', userController.login);
router.patch('/clearUserTasks', userController.clearUserTaskArray);
router.get('/profile', userController.getUserInfo);

module.exports = router;